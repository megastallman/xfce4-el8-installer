#!/bin/bash

sudo dnf install -y libsoup-devel

wget -c https://git.xfce.org/apps/xfce4-screenshooter/snapshot/xfce4-screenshooter-1.9.6.tar.gz
tar xf xfce4-screenshooter-1.9.6.tar.gz
cd xfce4-screenshooter-1.9.6
./autogen.sh --prefix=/usr
make
sudo make install
