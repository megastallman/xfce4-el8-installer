#!/bin/bash

wget -c https://git.xfce.org/apps/xfce4-notifyd/snapshot/xfce4-notifyd-0.4.4.tar.gz 
tar xf xfce4-notifyd-0.4.4.tar.gz
cd xfce4-notifyd-0.4.4
./autogen.sh --prefix=/usr
make
sudo make install
