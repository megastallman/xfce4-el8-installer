#!/bin/bash

# Documentation links:
#https://docs.xfce.org/xfce/building

sudo dnf groupinstall -y "Development Tools"
sudo dnf install -y gnome-tweak-tool glib2-devel libX11-devel gtk3-devel libSM-devel dbus-devel dbus-glib-devel startup-notification-devel gvfs-devel libnotify-devel libjpeg-turbo-devel libXi-devel  
[ -d "~/xfce" ] || mkdir -p ~/xfce/tmp

if [ -d "~/xfce/src" ]
then
    echo -e "\e[93m ===> Packages are already downloaded\e[0m"
else
    mkdir -p ~/xfce/src
    cd ~/xfce/src
    wget -c http://archive.xfce.org/xfce/4.14/fat_tarballs/xfce-4.14.tar.bz2
    tar xf xfce-4*
    cd src
    wget -c https://download.gnome.org/sources/libwnck/3.24/libwnck-3.24.1.tar.xz
    cd ../../..
fi


function buildpkg {
    cd ~/xfce
    rm -rf tmp &&  mkdir tmp && cd tmp
    tar xf ~/xfce/src/src/$1
    FIRSTDIR=$(ls -1 -d */ | head -n1)
    cd $FIRSTDIR
    echo -e "\e[93m ===> CHECKING $1 _____________________\e[0m"
    pwd
    ls
    echo -e "\e[93m ===> CONFIGURING $1 _____________________\e[0m"
    ./configure --prefix=/usr
    #./configure --prefix=${XFCE4_PREFIX}
    echo -e "\e[93m ===> BUILDING $1 _____________________\e[0m"
    make
    echo -e "\e[93m ===> INSTALLING $1 _____________________\e[0m"
    sudo make install
}


buildpkg libwnck-3.24.1.tar.xz
buildpkg xfce4-dev-tools-4.14.0.tar.bz2
buildpkg libxfce4util-4.14.0.tar.bz2
buildpkg xfconf-4.14.1.tar.bz2
buildpkg libxfce4ui-4.14.1.tar.bz2
buildpkg garcon-0.6.4.tar.bz2
buildpkg exo-0.12.8.tar.bz2
buildpkg xfce4-panel-4.14.0.tar.bz2
buildpkg Thunar-1.8.9.tar.bz2
buildpkg xfce4-settings-4.14.0.tar.bz2
buildpkg xfce4-session-4.14.0.tar.bz2
buildpkg xfwm4-4.14.0.tar.bz2
buildpkg xfdesktop-4.14.1.tar.bz2
buildpkg xfce4-appfinder-4.14.0.tar.bz2
buildpkg tumbler-0.2.7.tar.bz2
