#!/bin/bash

wget -c https://git.xfce.org/panel-plugins/xfce4-battery-plugin/snapshot/xfce4-battery-plugin-1.1.3.tar.gz 
tar xf xfce4-battery-plugin-1.1.3.tar.gz
cd xfce4-battery-plugin-1.1.3
./autogen.sh --prefix=/usr
make
sudo make install
