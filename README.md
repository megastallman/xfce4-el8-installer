# xfce4-EL8-installer

A simple script to install XFCE4 on EL8(Centos/RHEL/OEL)

Unfortunately these days Red Hat decided not to support any DE except Gnome 3, meanwhile Gnome suffers from poor design and poor UX from the very beginning. As a quick start, you can use the "Classic" Gnome session, but its implementation is also far from ideal. To convert it into something usable you can build XFCE4 from source.

1) Prepare a "Server with GUI" EL installation.
2) \# ./xfce4_builder.sh
3) \# reboot
