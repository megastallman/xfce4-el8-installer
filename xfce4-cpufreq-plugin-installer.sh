#!/bin/bash

wget -c https://git.xfce.org/panel-plugins/xfce4-cpufreq-plugin/snapshot/xfce4-cpufreq-plugin-1.2.1.tar.gz 
tar xf xfce4-cpufreq-plugin-1.2.1.tar.gz
cd xfce4-cpufreq-plugin-1.2.1/
./autogen.sh --prefix=/usr
make
sudo make install
